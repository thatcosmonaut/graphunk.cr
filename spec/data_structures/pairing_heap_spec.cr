require "../spec_helper"
require "../../src/graphunk/classes/data_structures/pairing_heap"

def with_pairing_heap
  heap = Graphunk::PairingHeap(String, Int32).new
  heap.insert("a", 8)
  heap.insert("b", 3)
  heap.insert("c", 2)
  yield heap
end

describe Graphunk::PairingHeap do
  # describe "array constructor" do
  #   array = [{"a", 5}, {"b", 3}, {"c", 1}, {"d", 10}, {"e", 15}]
  #   heap = Graphunk::PairingHeap(String, Int32).new(array)
  #
  #   it "should insert the elements into the heap" do
  #     heap.find_min.should eq({"c", 1})
  #   end
  # end

  describe "find_min" do
    context "heap is empty" do
      empty_heap = Graphunk::PairingHeap(String, Int32).new

      it "returns nil" do
        empty_heap.find_min.should be_nil
      end
    end

    context "heap is not empty" do
      it "returns the root element" do
        with_pairing_heap do |heap|
          heap.find_min.should eq({"c", 2})
        end
      end
    end
  end

  describe "delete_min" do
    it "deletes the minimum element" do
      with_pairing_heap do |heap|
        heap.delete_min
        heap.find_min.should eq({"b", 3})
      end
    end

    it "returns the minimum element" do
      with_pairing_heap do |heap|
        heap.delete_min.should eq({"c", 2})
      end
    end

    it "always returns the new minimum element" do
      heap = Graphunk::PairingHeap(String, Int32).new
      heap.insert("a", 8)
      heap.insert("b", 3)
      heap.insert("c", 2)
      heap.insert("d", 10)
      heap.insert("e", 20)
      heap.insert("f", 15)
      heap.insert("g", 1)

      heap.delete_min.should eq({"g", 1})
      heap.delete_min.should eq({"c", 2})
      heap.delete_min.should eq({"b", 3})
      heap.delete_min.should eq({"a", 8})
      heap.delete_min.should eq({"d", 10})
      heap.delete_min.should eq({"f", 15})
      heap.delete_min.should eq({"e", 20})
    end
  end

  describe "insert" do
    heap = Graphunk::PairingHeap(String, Int32).new
    heap.insert("a", 3)
    heap.insert("b", 2)

    it "adds the element" do
      heap.find_min.should eq({"b", 2})
    end
  end

  describe "adjust" do
    context "adjusting root" do
      it "re-orders the heap" do
        heap = Graphunk::PairingHeap(String, Int32).new
        heap.insert("a", 8)
        heap.insert("b", 3)
        node = heap.insert("c", 2)

        heap.adjust(node, 6)
        heap.find_min.should eq({"b", 3})
      end
    end

    context "adjusting non-root" do
      it "re-orders the heap" do
        heap = Graphunk::PairingHeap(String, Int32).new
        heap.insert("a", 5)
        heap.insert("b", 3)
        heap.insert("c", 8)
        node = heap.insert("d", 12)

        heap.adjust(node, 2)
        heap.delete_min.should eq({"d", 2})
        heap.delete_min.should eq({"b", 3})
        heap.delete_min.should eq({"a", 5})
        heap.delete_min.should eq({"c", 8})
      end
    end

  end
end
