require "../spec_helper"
require "../../src/graphunk/classes/data_structures/doubly_linked_list"

describe Graphunk::DoublyLinkedList do
  describe "empty?" do
    context "list is empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      it "returns true" do
        graph.empty?.should be_true
      end
    end

    context "list is non empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(3)
      it "returns false" do
        graph.empty?.should be_false
      end
    end
  end

  describe "first" do
    context "list is empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      it "raises exception" do
        expect_raises ArgumentError do
          graph.first
        end
      end
    end

    context "list is non empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(3)
      it "returns first element" do
        graph.first.should eq 1
      end
    end
  end

  describe "last" do
    context "list is empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      it "raises exception" do
        expect_raises ArgumentError do
          graph.first
        end
      end
    end

    context "list is non empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(3)
      graph.add(5)

      it "returns last element" do
        graph.last.should eq 5
      end
    end
  end

  describe "size" do
    context "list has two elements" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(3)
      graph.add(4)
      it "returns 2" do
        graph.size.should eq 2
      end
    end

    context "list had four elements and then one removed" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      node_to_remove = graph.add(1)
      graph.add(2)
      graph.add(3)
      graph.add(5)
      graph.remove(node_to_remove)
      it "returns 3" do
        graph.size.should eq 3
      end
    end
  end

  describe "[]" do
    context "index is out of range" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(2)

      it "raises exception" do
        expect_raises IndexError do
          graph[4]
        end
      end
    end

    context "index is in range" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(2)
      graph.add(3)

      it "returns value at index" do
        graph[2].should eq 3
      end
    end
  end

  describe "shift!" do
    context "index is out of range" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(2)

      it "raises exception" do
        expect_raises IndexError do
          graph.shift!(4)
        end
      end
    end

    context "index is in range" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(2)
      graph.add(3)

      it "removes the first n elements in place" do
        graph.shift!(2)
        graph.first.should eq 3
      end
    end
  end

  describe "shift" do
    context "index is out of range" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(2)

      it "raises exception" do
        expect_raises IndexError do
          graph.shift(4)
        end
      end
    end

    context "index is in range" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(2)
      graph.add(3)
      shifted = graph.shift(2)

      it "removes the first n elements" do
        shifted.first.should eq 3
      end

      it "is a different object" do
        graph.should_not eq shifted
      end
    end
  end

  describe "clone" do
    graph = Graphunk::DoublyLinkedList(Int32).new
    graph.add(1)
    graph.add(2)
    graph.add(3)

    it "creates a deep copy" do
      cloned = graph.clone

      cloned[0].should eq graph[0]
      cloned[1].should eq graph[1]
      cloned[2].should eq graph[2]

      cloned.should_not eq graph
    end
  end

  describe "add" do
    context "list is empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      it "has size one" do
        graph.size.should eq 1
      end

      it "has first element added" do
        graph.first.should eq 1
      end

      it "has last element added" do
        graph.last.should eq 1
      end
    end

    context "list is non empty" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(2)
      graph.add(3)
      graph.add(4)

      it "has first element unchanged" do
        graph.first.should eq 1
      end

      it "has new last element" do
        graph.last.should eq 4
      end
    end
  end

  describe "remove" do
    context "node does not belong to list" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      other = Graphunk::DoublyLinkedList(Int32).new
      other_node = other.add(1)

      it "raises ArgumentError" do
        expect_raises ArgumentError do
          graph.remove(other_node)
        end
      end
    end

    context "node is the sole node" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      sole = graph.add(1)

      it "should be empty" do
        graph.remove(sole)
        graph.empty?.should be_true
      end
    end

    context "node is the root node" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      root = graph.add(1)
      graph.add(3)
      graph.add(5)

      it "sets a new root node" do
        graph.remove(root)
        graph.first.should eq 3
      end
    end

    context "node is the tail node" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      graph.add(3)
      tail = graph.add(5)

      it "sets a new tail node" do
        graph.remove(tail)
        graph.last.should eq 3
      end
    end

    context "node is neither root nor tail" do
      graph = Graphunk::DoublyLinkedList(Int32).new
      graph.add(1)
      mid = graph.add(3)
      graph.add(4)
      graph.add(5)

      it "should remove the middle node" do
        graph.remove(mid)

        graph.size.should eq 3
        graph[0].should eq 1
        graph[1].should eq 4
        graph[2].should eq 5
      end
    end
  end
end
