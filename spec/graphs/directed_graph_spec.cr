require "../spec_helper"
require "../../src/graphunk/classes/graphs/directed_graph"

def with_directed_graph
  yield Graphunk::DirectedGraph(String).new(
    ["a", "b", "c", "d"],
    [{"a", "b"}, {"a", "c"}, {"b", "a"}, {"b", "d"}]
  )
end

describe Graphunk::DirectedGraph do
  describe "vertices" do
    it "returns a list of all vertices" do
      with_directed_graph do |graph|
        graph.vertices.should match_array ["a", "b", "c", "d"]
      end
    end
  end

  describe "edges" do
    it "returns a list of all edges" do
      with_directed_graph do |graph|
        graph.edges.should match_array [{"a", "b"}, {"a", "c"}, {"b", "a"}, {"b", "d"}]
      end
    end
  end

  describe "add_vertex" do
    context "vertex does not exist" do
      it "adds a vertex to the graph" do
        with_directed_graph do |graph|
          graph.add_vertex("e")
          graph.vertices.should match_array ["a", "b", "c", "d", "e"]
        end
      end
    end

    context "vertex already exists" do
      it "raises an ArgumentError" do
        with_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.add_vertex("a")
          end
        end
      end
    end
  end

  describe "add_vertices" do
    context "vertices do not exist" do
      it "adds the vertices to the graph" do
        with_directed_graph do |graph|
          graph.add_vertices("g", "h", "i")
          graph.vertices.should match_array ["a", "b", "c", "d", "g", "h", "i"]
        end
      end
    end

    context "one of the vertices exists in the graph" do
      it "raises an ArgumentError" do
        with_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.add_vertices("g", "h", "a")
          end
        end
      end
    end
  end

  describe "add_edge" do
    context "edge does not exist" do
      it "adds an edge to the graph" do
        with_directed_graph do |graph|
          graph.add_edge("c", "a")
          graph.edges.should match_array [{"a", "b"}, {"a", "c"}, {"b", "a"}, {"b", "d"}, {"c", "a"}]
        end
      end
    end

    context "edge already exists" do
      it "raises an ArgumentError" do
        with_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.add_edge("a", "b")
          end
        end
      end
    end
  end

  describe "remove_edge" do
    context "edge exists" do
      it "removes an edge from the graph" do
        with_directed_graph do |graph|
          graph.remove_edge("a", "b")
          graph.edges.should match_array [{"a", "c"}, {"b", "a"}, {"b", "d"}]
        end
      end
    end

    context "edge does not exist" do
      it "raises an ArgumentError" do
        with_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_edge("a", "d")
          end
        end
      end
    end
  end

  describe "remove_vertex" do
    context "vertex exists" do
      with_directed_graph do |graph|
        graph.remove_vertex("b")

        it "removes a vertex from the graph" do
          graph.vertices.should match_array ["a", "c", "d"]
        end

        it "removes edges containing the vertex from the graph" do
          graph.edges.should match_array [{"a", "c"}]
        end
      end
    end

    context "vertex does not exist" do
      it "raises an ArgumentError" do
        with_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.remove_edge("d", "e")
          end
        end
      end
    end
  end

  describe "edges_on_vertex" do
    with_directed_graph do |graph|
      context "vertex exists" do
        it "returns a list of all edges containing the input vertex" do
          graph.edges_on_vertex("a").should match_array [{"a", "b"}, {"a", "c"}, {"b", "a"}]
        end
      end

      context "vertex does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.edges_on_vertex("z")
          end
        end
      end
    end
  end

  describe "neighbors" do
    with_directed_graph do |graph|
      context "vertex exists" do
        it "returns a list containing the neighbors of the given vertex" do
          graph.neighbors("a").should match_array ["b", "c"]
        end
      end

      context "vertex does not exist" do
        it "raises an ArgumentError" do
          expect_raises ArgumentError do
            graph.neighbors("e")
          end
        end
      end
    end
  end

  describe "edge_exists?" do
    with_directed_graph do |graph|
      context "edge exists" do
        it "returns true" do
          graph.edge_exists?("a", "b").should be_true
        end
      end

      context "edge does not exist" do
        it "returns false" do
          graph.edge_exists?("a", "d").should be_false
        end
      end
    end
  end

  describe "vertex_exists?" do
    with_directed_graph do |graph|
      context "vertex exists" do
        it "returns true" do
          graph.vertex_exists?("a").should be_true
        end
      end

      context "vertex does not exist" do
        it "returns false" do
          graph.vertex_exists?("t").should be_false
        end
      end
    end
  end

  describe "transpose" do
    it "returns a graph which is the transpose of the current graph" do
      with_directed_graph do |graph|
        graph.transpose.edges.should match_array [{"a", "b"}, {"b", "a"}, {"c", "a"}, {"d", "b"}]
      end
    end
  end

  describe "tranpose!" do
    it "tranposes the graph in-place" do
      with_directed_graph do |graph|
        graph.transpose!
        graph.edges.should match_array [{"a", "b"}, {"b", "a"}, {"c", "a"}, {"d", "b"}]
      end
    end
  end

  describe "reachable_by_two_path" do
    context "vertex exists" do
      it "returns a list of all vertices reachable from the input vertex with a 2-path" do
        with_directed_graph do |graph|
          graph.add_vertex("e")
          graph.reachable_by_two_path("a").should match_array ["a", "b", "c", "d"]
        end
      end
    end

    context "vertex does not exist" do
      it "raises an ArgumentError" do
        with_directed_graph do |graph|
          expect_raises ArgumentError do
            graph.reachable_by_two_path("z")
          end
        end
      end
    end
  end

  describe "square" do
    it "returns a graph which is the square of the graph" do
      with_directed_graph do |graph|
        graph.square.edges.should match_array [{"a", "b"}, {"a", "c"}, {"b", "a"}, {"b", "c"}, {"b", "d"}, {"a", "d"}]
      end
    end
  end

  describe "dfs" do
    it "returns a hash containing depth-first start and finish times for each vertex" do
      graph = Graphunk::DirectedGraph(String).new(["a", "b", "c", "d"],
        [{"a", "b"}, {"a", "c"}, {"b", "d"}])

      result = {"a" => {:start => 1, :finish => 8},
                "b" => {:start => 2, :finish => 5},
                "c" => {:start => 6, :finish => 7},
                "d" => {:start => 3, :finish => 4},
      }

      graph.dfs.should eq result
    end
  end

  describe "topological sort" do
    context "connected graph" do
      it "returns a valid topological ordering on the graph" do
        with_directed_graph do |graph|
          graph.topological_sort.should eq ["a", "c", "b", "d"]
        end
      end
    end

    context "unconnected graph" do
      it "returns a valid topological ordering on the graph" do
        graph = Graphunk::DirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g", "t", "m"],
          [{"a", "b"}, {"a", "c"}, {"a", "d"}, {"b", "f"}, {"b", "g"}, {"c", "g"}, {"e", "t"}, {"t", "m"}])

        graph.topological_sort.should match_array ["e", "t", "m", "a", "d", "c", "b", "g", "f"]
      end
    end
  end

  describe "transitive?" do
    context "the orientation is transitive" do
      graph = Graphunk::DirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g"],
        [{"a", "b"}, {"a", "g"}, {"c", "d"}, {"e", "d"}, {"f", "d"}, {"f", "e"}, {"f", "g"}])

      it "returns true" do
        graph.transitive?.should be_true
      end
    end

    context "the orientation is not transitive" do
      graph = Graphunk::DirectedGraph(String).new(["a", "b", "c", "d", "e", "f", "g"],
        [{"a", "b"}, {"b", "c"}, {"c", "d"}, {"d", "e"}, {"e", "f"}, {"f", "g"}, {"g", "a"}])

      it "returns false" do
        graph.transitive?.should be_false
      end
    end
  end
end
