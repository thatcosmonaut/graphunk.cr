module Graphunk
  class DisjointSet(T)
    def initialize
      @rank = Hash(T, Int32).new
      @parent = Hash(T, T).new
    end

    getter :members

    def make_set(x)
      @rank[x] = 0
      @parent[x] = x
    end

    def find(x)
      if @parent[x] != x
        @parent[x] = find(@parent[x])
      end
      @parent[x]
    end

    def union(x, y)
      x_root = find(x)
      y_root = find(y)

      return if x_root == y_root

      if @rank[x_root] < @rank[y_root]
        @parent[x_root] = y_root
      elsif @rank[x_root] > @rank[y_root]
        @parent[y_root] = x_root
      else
        @parent[x_root] = y_root
        @rank[y_root] += 1
      end
    end
  end
end
