require "../../enums"
require "../../classes/data_structures/disjoint_set"
require "../../classes/data_structures/pairing_heap"
require "../../modules/weighted_graph"
require "../../modules/undirected_graph_methods"

module Graphunk
  class WeightedUndirectedGraph(T)
    include WeightedGraph(T)
    include UndirectedGraphMethods(T)

    def initialize(@vertices = [] of T, @weights = {} of Tuple(T, T) => Int32)
      @neighbors = Hash(T, Set(T)).new
      @edges = @weights.keys
      @vertices.each do |vertex|
        @neighbors[vertex] = Set(T).new
      end
      @edges.each do |edge|
        add_neighbors(edge)
      end
    end

    def add_edge(edge : Tuple(T, T), weight)
      if edge_exists?(edge)
        raise ArgumentError.new("This edge already exists")
      elsif vertex_exists?(edge.first) && vertex_exists?(edge.last)
        @edges << edge
        @weights[edge] = weight
        add_neighbors(edge)
      else
        raise ArgumentError.new("One of the vertices referenced does not exist in the graph")
      end
    end

    def add_edge(v, u, w)
      add_edge(order_vertices(v, u), w)
    end

    def remove_edge(edge)
      if edge_exists?(edge)
        @edges.delete edge
        remove_weight(edge)
        @neighbors[edge.first].delete edge.last
        @neighbors[edge.last].delete edge.first
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def remove_edge(v, u)
      remove_edge(order_vertices(v, u))
    end

    private def remove_weight(edge)
      @weights.delete edge
    end

    def edge_weight(edge)
      if edge_exists?(edge)
        @weights[edge]
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def edge_weight(v, u)
      edge_weight(order_vertices(v, u))
    end

    def adjust_weight(v, u, w)
      if edge_exists?(v, u)
        @weights[order_vertices(v, u)] = w
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def minimum_spanning_tree(algorithm = Algorithms::Prim)
      case algorithm
      when Algorithms::Prim
        prim_minimum_spanning_tree
      when Algorithms::Kruskal
        kruskal_minimum_spanning_tree
      else
        raise ArgumentError.new("Need this here so type signature doesn't contain nil")
      end
    end

    private def prim_minimum_spanning_tree
      cheapest = {} of T => Int32
      edge_map = {} of T => Tuple(T, T) | Nil
      pairing_heap = PairingHeap(T, Int32).new
      heap_nodes = {} of T => PairingHeapNode(T, Int32)

      vertices.each do |v|
        cheapest[v] = Int32::MAX
        edge_map[v] = nil
        heap_nodes[v] = pairing_heap.insert(v, Int32::MAX)
      end

      open_set = vertices.to_set

      forest = WeightedUndirectedGraph(T).new

      until open_set.empty?
        minimum_vertex, minimum_weight = pairing_heap.delete_min
        open_set.delete minimum_vertex
        forest.add_vertex(minimum_vertex)
        if (edge = edge_map[minimum_vertex])
          forest.add_edge(edge.as(Tuple(T, T)), edge_weight(edge))
        end
        neighbors(minimum_vertex).each do |neighbor|
          weight = edge_weight(minimum_vertex, neighbor)
          if open_set.includes?(neighbor) && weight < cheapest[neighbor]
            cheapest[neighbor] = weight
            pairing_heap.adjust(heap_nodes[neighbor], weight)
            edge_map[neighbor] = order_vertices(minimum_vertex, neighbor)
          end
        end
      end

      forest
    end

    private def kruskal_minimum_spanning_tree
      forest = WeightedUndirectedGraph(T).new
      disjoint_set = DisjointSet(T).new

      vertices.each do |vertex|
        forest.add_vertex(vertex)
        disjoint_set.make_set(vertex)
      end

      weights.to_a.sort_by { |edge, weight| weight }.each do |edge, weight|
        if disjoint_set.find(edge.first) != disjoint_set.find(edge.last)
          forest.add_edge(edge, weight)
          disjoint_set.union(edge.first, edge.last)
        end
      end

      forest
    end
  end
end
