require "../../enums"
require "../../exceptions"
require "../../modules/weighted_graph"
require "../../modules/directed_graph_methods"

module Graphunk
  class WeightedDirectedGraph(T)
    include WeightedGraph(T)
    include DirectedGraphMethods(T)

    def initialize(@vertices = [] of T, @weights = {} of Tuple(T, T) => Int32)
      @neighbors = Hash(T, Set(T)).new
      @edges = @weights.keys
      @vertices.each do |vertex|
        @neighbors[vertex] = Set(T).new
      end
      @edges.each do |edge|
        @neighbors[edge.first] << edge.last
      end
    end

    def add_edge(edge, weight)
      if edge_exists?(edge)
        raise ArgumentError.new("This edge already exists")
      elsif vertex_exists?(edge.first) && vertex_exists?(edge.last)
        @edges << edge
        @weights[edge] = weight
      else
        raise ArgumentError.new("One of the vertices referenced does not exist in the graph")
      end
    end

    def add_edge(v, u, w)
      add_edge({v, u}, w)
    end

    def remove_edge(edge)
      if edge_exists?(edge)
        @edges.delete edge
        @neighbors[edge.first].delete edge.last
        @weights.delete edge
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def remove_edge(v, u)
      remove_edge({v, u})
    end

    def edge_weight(edge)
      if edge_exists?(edge)
        @weights[edge]
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def edge_weight(v, u)
      edge_weight({v, u})
    end

    def adjust_weight(edge, weight)
      if edge_exists?(edge)
        @weights[edge] = weight
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def adjust_weight(v, u, w)
      adjust_weight({v, u}, w)
    end

    def shortest_path_distance(v, u, algorithm = Algorithms::Dijkstra)
      if !vertex_exists?(v) || !vertex_exists?(u)
        raise ArgumentError.new("A specified vertex does not exist in the graph")
      end

      case algorithm
      when Algorithms::Dijkstra
        if @weights.values.any? { |w| w < 0 }
          raise NegativeWeightException.new("The graph contains a negative weight. Try Graphunk::Algorithms::SingleSourceShortestPath::BellmanFord")
        end
        dijkstra_shortest_path_distance(v, u)
      when Algorithms::BellmanFord
        bellman_ford_shortest_path_distance(v, u)
      end
    end

    def shortest_path(v, u, algorithm = Algorithms::Dijkstra)
      if !vertex_exists?(v) || !vertex_exists?(u)
        raise ArgumentError.new("A specified vertex does not exist in the graph")
      end

      case algorithm
      when Algorithms::Dijkstra
        if @weights.values.any? { |w| w < 0 }
          raise NegativeWeightException.new("The graph contains a negative weight. Try Graphunk::Algorithms::SingleSourceShortestPath::BellmanFord")
        end
        dijkstra_shortest_path(v, u)
      when Algorithms::BellmanFord
        bellman_ford_shortest_path(v, u)
      end
    end

    macro def_shortest_path_methods(method_name_prefix)
      private def {{method_name_prefix}}_shortest_path(v, u)
        ([] of T).tap do |s|
          previous = {{method_name_prefix}}_single_source_shortest_path_previous(v)
          while previous[u]
            s.insert(0, u.as(T))
            u = previous[u]
          end
          return nil if u != v
          s.insert(0, v)
        end
      end

      private def {{method_name_prefix}}_shortest_path_distance(v, u)
        result = {{method_name_prefix}}_single_source_shortest_path_distances(v)[u]
        result == Int32::MAX ? nil : result
      end

      private def {{method_name_prefix}}_single_source_shortest_path_distances(source)
        {{method_name_prefix}}_single_source_shortest_path(source)[:distance]
      end

      private def {{method_name_prefix}}_single_source_shortest_path_previous(source)
        {{method_name_prefix}}_single_source_shortest_path(source)[:previous]
      end
    end

    def_shortest_path_methods(dijkstra)

    private def dijkstra_single_source_shortest_path(source)
      distance = {} of T => Int32
      previous = {} of T => T | Nil
      vertices.each do |vertex|
        distance[vertex] = Int32::MAX
        previous[vertex] = nil
      end

      distance[source] = 0
      q = vertices.clone

      until q.empty?
        u = q.delete(q.min_by { |vertex| distance[vertex] })
        break if distance[u] == Int32::MAX

        neighbors(u).each do |v|
          alt = distance[u] + edge_weight(u, v)
          if alt < distance[v]
            distance[v] = alt
            previous[v] = u
          end
        end
      end

      {distance: distance, previous: previous}
    end

    def_shortest_path_methods(bellman_ford)

    private def bellman_ford_single_source_shortest_path(source)
      distance = {} of T => Int32
      previous = {} of T => T | Nil
      vertices.each do |vertex|
        distance[vertex] = Int32::MAX
        previous[vertex] = nil
      end
      distance[source] = 0

      vertices.size.times do
        weights.each do |edge, weight|
          if distance[edge.first] + weight < distance[edge.last]
            distance[edge.last] = distance[edge.first] + weight
            previous[edge.last] = edge.first
          end
        end
      end

      weights.each do |edge, weight|
        if distance[edge.first] + weight < distance[edge.last]
          raise NegativeCycleException.new("The graph contains a negative cycle")
        end
      end

      {distance: distance, previous: previous}
    end
  end
end
