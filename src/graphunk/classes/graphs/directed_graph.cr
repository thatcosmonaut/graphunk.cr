require "../../modules/directed_graph_methods"

module Graphunk
  class DirectedGraph(T)
    include Graph(T)
    include DirectedGraphMethods(T)

    def initialize(@vertices = [] of T, @edges = [] of Tuple(T, T))
      @neighbors = Hash(T, Set(T)).new
      @vertices.each do |vertex|
        @neighbors[vertex] = Set(T).new
      end
      @edges.each do |edge|
        @neighbors[edge.first] << edge.last
      end
    end

    def add_edge(first_vertex, second_vertex)
      if edge_exists?(first_vertex, second_vertex)
        raise ArgumentError.new("This edge already exists")
      elsif vertex_exists?(first_vertex) && vertex_exists?(second_vertex)
        @edges << {first_vertex, second_vertex}
        @neighbors[first_vertex] << second_vertex
      else
        raise ArgumentError.new("One of the vertices referenced does not exist in the graph")
      end
    end

    def remove_edge(edge)
      if edge_exists?(edge)
        @edges.delete(edge)
        @neighbors[edge.first].delete edge.last
      else
        raise ArgumentError.new("That edge does not exist in the graph")
      end
    end

    def remove_edge(first_vertex, second_vertex)
      remove_edge({first_vertex, second_vertex})
    end
  end
end
