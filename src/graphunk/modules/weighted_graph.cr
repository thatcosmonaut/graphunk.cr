require "./graph"

module Graphunk
  module WeightedGraph(T)
    include Graph(T)

    getter :weights
  end
end
