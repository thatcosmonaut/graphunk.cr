# TODO: convenience methods for all edge-related methods (allow calling with either edge tuple or two vertices)
# TODO: edge validation on construction

module Graphunk
  module Graph(T)
    def initialize(@vertices = [] of T, @edges = [] of Tuple(T, T))
      @neighbors = Hash(T, Set(T)).new
    end

    def_clone
    getter :vertices
    getter :edges

    def add_vertex(vertex)
      unless vertex_exists?(vertex)
        @vertices << vertex
        @neighbors[vertex] = Set(T).new
      else
        raise ArgumentError.new("Vertex already exists")
      end
    end

    def add_vertices(names : Array(T))
      if !names.to_set.intersects?(vertices.to_set)
        names.each { |name| add_vertex(name) }
      else
        raise ArgumentError.new("One or more of the given vertices already exists")
      end
    end

    def add_vertices(*names)
      add_vertices(names.to_a)
    end

    def remove_vertex(vertex)
      edges_to_remove = [] of Tuple(T, T)
      if vertex_exists?(vertex)
        edges.each do |edge|
          edges_to_remove << edge if edge.includes?(vertex)
        end

        edges_to_remove.each do |edge|
          remove_edge(edge)
        end

        @vertices.delete vertex
        @neighbors.delete vertex
      else
        raise ArgumentError.new("That vertex does not exist in the graph")
      end
    end

    def neighbors(vertex)
      if vertex_exists?(vertex)
        @neighbors[vertex].to_a
      else
        raise ArgumentError.new("That vertex does not exist in the graph")
      end
    end

    def edges_on_vertex(vertex)
      if vertex_exists?(vertex)
        edges.select { |edge| edge.includes?(vertex) }
      else
        raise ArgumentError.new("That vertex does not exist in the graph")
      end
    end

    def edge_exists?(edge)
      edges.includes?(edge)
    end

    def vertex_exists?(vertex)
      vertices.includes?(vertex)
    end

    def degree(vertex)
      neighbors(vertex).size
    end
  end
end
