require "./graph"

module Graphunk
  module UndirectedGraphMethods(T)
    include Graph(T)

    private def add_neighbors(edge)
      @neighbors[edge.first] << edge.last
      @neighbors[edge.last] << edge.first
    end

    private def order_vertices(v, u)
      v < u ? {v, u} : {u, v}
    end

    def edge_exists?(v, u)
      edge_exists?(order_vertices(v, u))
    end

    def lexicographic_bfs
      sets = [vertices.clone]
      output_vertices = [] of T

      until sets.empty?
        v = sets.first.delete_at(0)
        sets.delete_at(0) if sets.first.empty?
        output_vertices << v
        replaced = [] of Array(T)
        neighbors(v).each do |neighbor|
          if (s = sets.find { |set| set.includes?(neighbor) })
            s_index = ->{ sets.each_with_index { |x, i| return i if x == s } || -1 }.call
            if replaced.includes?(s)
              t = sets[s_index - 1]
            else
              t = [] of T
              sets.insert(s_index, t)
              replaced << s.as(Array(T))
            end
            s.as(Array(T)).delete(neighbor)
            t << neighbor
            sets.delete(s) if s.as(Array(T)).empty?
          end
        end
      end

      output_vertices
    end

    def clique?(vertex_list)
      clique = true
      vertex_list.each do |vertex|
        unless (neighbors(vertex) & vertex_list).sort == (vertex_list - [vertex]).sort
          clique = false
          break
        end
      end
      clique
    end

    def chordal?
      chordal = true
      (lexicographic_ordering = lexicographic_bfs.reverse).each_with_index do |v, i|
        successors_of_v = lexicographic_ordering[i..-1]
        unless clique?([v] | (neighbors(v) & successors_of_v))
          chordal = false
          break
        end
      end
      chordal
    end

    def complete?
      n = vertices.size
      edges.size == (n * (n - 1) / 2)
    end

    def bipartite?
      colors = {} of String => String
      d = {} of String => Int32
      partition = {} of String => Int32
      vertices.each do |vertex|
        colors[vertex] = "white"
        d[vertex] = Int32::MAX
        partition[vertex] = 0
      end

      start = vertices.first
      colors[start] = "gray"
      partition[start] = 1
      d[start] = 0

      stack = [] of String
      stack.push(start)

      until stack.empty?
        vertex = stack.pop
        neighbors(vertex).each do |neighbor|
          if partition[neighbor] == partition[vertex]
            return false
          else
            if colors[neighbor] == "white"
              colors[neighbor] == "gray"
              d[neighbor] = d[vertex] + 1
              partition[neighbor] = 3 - partition[vertex]
              stack.push(neighbor)
            end
          end
        end
        stack.pop
        colors[vertex] = "black"
      end

      true
    end

    def comparability?
      assign_orientation ? true : false
    end

    def transitive_orientation
      assign_orientation
    end

    private def assign_orientation
      transitive_orientation = Graphunk::DirectedGraph(T).new
      transitive_orientation.add_vertices(vertices)

      unconsidered_edges = edges.clone

      transitive = true

      until unconsidered_edges.empty?
        considered_edge = unconsidered_edges.first
        unconsidered_edges.delete(considered_edge)

        transitive_orientation.add_edge(considered_edge.first, considered_edge.last)

        explore = uninitialized Tuple(T, T) -> Void
        explore = ->(edge : Tuple(T, T)) do
          edge_set = false
          adjacent_edges(edge.first, edge.last).each do |adjacent_edge|
            next if unconsidered_edges.includes? adjacent_edge

            shared_vertex = (edge.to_set & adjacent_edge.to_set).first
            unshared_edge_vertex = edge.reject { |vertex| adjacent_edge.includes? vertex }.first
            unshared_adjacent_edge_vertex = adjacent_edge.reject { |vertex| edge.includes? vertex }.first

            unless edge_exists?(unshared_edge_vertex, unshared_adjacent_edge_vertex)
              if transitive_orientation.edge_exists?(shared_vertex, unshared_adjacent_edge_vertex)
                transitive = false if transitive_orientation.edge_exists?(unshared_edge_vertex, shared_vertex)
                transitive_orientation.add_edge(shared_vertex, unshared_edge_vertex) unless transitive_orientation.edge_exists?(shared_vertex, unshared_edge_vertex)
                unconsidered_edges.delete(order_vertices(shared_vertex, unshared_edge_vertex))
                edge_set = true
              else
                transitive = false if transitive_orientation.edge_exists?(shared_vertex, unshared_edge_vertex)
                transitive_orientation.add_edge(unshared_edge_vertex, shared_vertex) unless transitive_orientation.edge_exists?(unshared_edge_vertex, shared_vertex)
                unconsidered_edges.delete(order_vertices(shared_vertex, unshared_edge_vertex))
                edge_set = true
              end
            end
          end

          adjacent_edges(edge.first, edge.last).each { |neighbor| explore.call neighbor if unconsidered_edges.includes? neighbor } if edge_set
        end

        adjacent_edges(considered_edge.first, considered_edge.last).each { |neighbor_edge| explore.call neighbor_edge }
      end

      return transitive ? transitive_orientation : nil
    end
  end
end
